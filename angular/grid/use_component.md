import { Grid } from 'grid.component';

@ViewChild('gridSlider') gridSlider: Grid;

private grid: any = {
  animation: true,				// true ? se mostrará la animación de las lineas : no hay lineas
  colorDots: "#ffffff",		// color de los puntos del grid
  full: true,							// true ? el tamaño del grid solo es del tamaño de la pantalla : será tan grande como el contenedor
  elements: [
    {id:"slide"}, 				// id:: id de los elementos a ignorar (no se mostrará el gird en esa sección)
    {id:"scrollDown", surplus: 2}	// surplus:: margen de error (si se desea que se tome en cuenta o no ciertos pixeles)
  ]
};

<grid #gridSlider [anim]="grid" ></grid>

ngAfterViewInit(){
	let _ = this;	
  _.paintGridBlack(null);
}
<!--  -->
private paintGridBlack(e){
  let _ = this;
  if(e){
    _.gridBlack.getSizeX();
    _.calcGrid() // calculos que interfieren con la altura del contenedor o la posición de las sombras
    _.gridBlack.getSizeY();
    _.gridBlack.generateGrid();
  }else{
    let img01Loaded = false;
    let interval = setInterval(function(){
      img01Loaded = _.img01.complete;
      if(img01Loaded){
        clearInterval(interval);
        _.calcGrid(); // calculos que interfieren con la altura del contenedor o la posición de las sombras
        _.gridBlack.getSizeY();
        _.gridBlack.generateGrid();
        setTimeout(function(){	// Remover loader
          let state = _.loaderService.getState();
          state.open = true;
        },500);
      }
    }, 10);
	}
}
private resize(e){
  let _ = this;
  _.paintGridBlack(e);
}