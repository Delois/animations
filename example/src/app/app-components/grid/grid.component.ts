import { Component, AfterViewInit, ViewChild, Input, ElementRef, NgZone} from '@angular/core';
import { Observable } from 'rxjs/Rx';

@Component({
	selector: "grid",
  templateUrl: './grid.component.html'
})
	
export class GridComponent implements AfterViewInit{

	constructor(private ngZone: NgZone) {}

  @ViewChild('canvas') elemCanvas: ElementRef; 
	@ViewChild('canvasLines') canvasLinesRef: ElementRef; 
	@Input("config") config_grid: any = {};

	private canvas: any = null; // canvas del grid
  private ctx: any = null;  // context del canvas del grid
  public size: any = {  // ancho del navegador y alto del contenedor
    x: 0,
    y: 0
  };
  public numDot: any = {  // número de puntos sobre el eje X y número de puntos sobre el eje Y
    x: 0,
    y: 0
  };
  public distance: number = 0; // distancia entre punto y punto
  public residY: number = 0;  // distancia entre el primer punto y el borde superior del contenedor, aplica de la misma forma para el ultimo punto y el borde inferior del contendor
  private exclude: any = []; // Arreglo con elementos del DOM que serán ignorados por el grid
  private areaTxt: any[] = []; // Arreglo con coordenadas de los objetos a ignorar por el grid
  private arrayDots: any = []; // Matriz con coordenadas de cada punto

  private canvasLines: any = null; // canvas de lineas con animación
  private ctxLines: any = null;  // context del canvas de lineas de animación 
  private animationId: any = null; // id del setinterval que creará la animación de las lineas
  private createLineId: any = null; // id del setinterval que creará una nueva linea
  private lines: any = []; // Arreglo de objetos con información de cada linea animada
  private requestAnimationFrame: any = window.requestAnimationFrame;

  private widthWindow: number = window.innerWidth;
  
  ngAfterViewInit(){
    let _ = this;
    if(_.elemCanvas){ _.initCanvas(); }
    _.getSizeX();
  }
  ngOnDestroy(){
    let _ = this;
    window.cancelAnimationFrame(_.animationId);
    clearTimeout(_.createLineId);
  }
  // Inicializa el canvas(grid)
  private initCanvas(){
    let _ = this;
    let android = navigator.userAgent.match(/Android/i);
    _.canvas = _.elemCanvas.nativeElement;
    _.ctx = _.canvas.getContext('2d');
    for (let i = 0; i < _.config_grid.elements.length; i++) {
      let elem = document.getElementById(_.config_grid.elements[i].id);
      if(elem){_.exclude.push(elem);}
    }
    if(_.config_grid.animation && !android){ _.animationLines(); }
  }
  // Calcula en ancho del grid y el número de puntos a lo ancho de la pantalla
  public getSizeX(){
    let _ = this;
    _.size.x = _.canvas.offsetWidth;
    let distancePoint = _.size.x > 1024 ? 85 : _.size.x > 640 ? 65 : 50; // distancia inicial entre los puntos
    let prevDotsX = Math.round(_.size.x / distancePoint);
    _.widthWindow = window.innerWidth;
    _.numDot.x = prevDotsX % 2 === 0 ? prevDotsX + 1 : prevDotsX;
    _.distance = _.size.x / _.numDot.x;
    if(!_.config_grid.full){ _.residY = _.distance / 2; }
  }
  public getSizeY(){
    let _ = this;
    _.size.y = _.canvas.offsetHeight;
    _.numDot.y = Math.round(_.size.y / _.distance);
    if (_.config_grid.full) {
      _.residY = ((_.size.y - (_.numDot.y * _.distance)) / 2) + (_.distance / 2);
    }else{
      _.residY = _.distance / 2;
    }
  }
  public generateGrid(){
    let _ = this;
    _.stopAnimation();
    if(_.config_grid.elements.length > 0){ _.areaTxt = []; }
    _.drawPoints();
    if(_.config_grid.animation){ _.resizeAnimation(); }
  }

  private drawPoints(){
    var _ = this;
    _.ctx.clearRect(0, 0, _.size.x, _.size.y);
    _.ctx.canvas.width = _.size.x;
    _.ctx.canvas.height = _.size.y;
    _.sortElems();
    _.sortLines();
    _.createGrid();
  }
  private sortElems(){
    let _ = this;
    for (let i = 0; i < _.exclude.length; i++) { // obtiene las posiciones de los elementos a ignorar
      let elem = _.config_grid.elements[i];
      let excludeWt = _.exclude[i].offsetWidth;
      let excludeHt = _.exclude[i].offsetHeight;
      let marginErr = elem.surplus ? elem.surplus : 0;
      _.areaTxt.push({
        top: _.exclude[i].offsetTop + marginErr,
        bot: _.exclude[i].offsetTop + excludeHt - marginErr,
        right: excludeWt + _.exclude[i].offsetLeft - marginErr,
        letf: _.exclude[i].offsetLeft + marginErr
      });
    }
  }
  private createGrid(){ // genera el grid
    let _ = this;
    _.arrayDots = [];
    for (let i = 0; i < _.numDot.x; i++) {
      _.arrayDots.push([]);
      for (let j = 0; j < _.numDot.y; j++) {
        let dotValid = {center: _.drawDot(i, j, _.config_grid.colorDots)};
        _.arrayDots[i].push(dotValid);
      }
    }
  }  
  private sortLines(){ // genera las lineas estaticas
    let _ = this;
    if(!_.config_grid.lines){return}
    for (let i = 0; i < _.config_grid.lines.length; ++i) {
      _.drawLine(
        _.config_grid.lines[i].initX < 0 ? _.numDot.x + _.config_grid.lines[i].initX : _.config_grid.lines[i].initX,
        _.config_grid.lines[i].initY < 0 ? _.numDot.y + _.config_grid.lines[i].initY : _.config_grid.lines[i].initY,
        _.config_grid.lines[i].finX < 0 ? _.numDot.x + _.config_grid.lines[i].finX : _.config_grid.lines[i].finX,
        _.config_grid.lines[i].finY < 0 ? _.numDot.y + _.config_grid.lines[i].finY : _.config_grid.lines[i].finY,
      );
    }
  }
  private drawDot(x, y, clr){
    let _ = this;
    let posX = Math.round((x * _.distance) + (_.distance / 2));
    let posY = Math.round((y * _.distance) + _.residY);
    let add = true;
    let wtGrid = _.size.x > 1024 ? 12 : _.size.x > 640 ? 8 : 4;
    for (let i = 0; i < _.areaTxt.length; ++i) {
      if(_.areaTxt[i].top <= posY && _.areaTxt[i].bot >= posY && 
        _.areaTxt[i].letf <= posX && _.areaTxt[i].right >= posX){
        add = false;
        return null;
      }
    }
    if(add){
      _.ctx.beginPath();
      _.ctx.strokeStyle = clr;
      _.ctx.fillStyle = clr;
      _.ctx.arc(posX, posY, (clr === "#fa6a00" ? wtGrid : (_.size.x > 640 ? 2 : 1)), 0, Math.PI * 2);
      _.ctx.fill();
      _.ctx.stroke();
    }
    return {x:posX, y:posY};
  }
  private drawLine(x1, y1, x2, y2){
    var _ = this;
    _.ctx.beginPath();
    _.ctx.strokeStyle = "#ffffff";
    _.ctx.moveTo(
      (_.distance * x1) + _.distance / 2, 
      (_.distance * y1) + _.residY
    );
    _.ctx.lineTo(
      (_.distance * x2) + _.distance / 2, 
      (_.distance * y2) + _.residY
    );
    _.ctx.lineWidth = 2;
    _.ctx.stroke();
  }
 

// Animación de lineas
  private animationLines(){
    let _ = this;
    window.requestAnimationFrame = _.requestAnimationFrame;
    _.canvasLines = _.canvasLinesRef.nativeElement;
    _.ctxLines = _.canvasLines.getContext('2d');
  }
  private resizeAnimation(){ // Reinicia la animación de las lineas despues de un resize
    let _ = this;
    _.ctxLines.canvas.width = _.size.x;
    _.ctxLines.canvas.height = _.size.y;
    _.ctxLines.clearRect(0, 0, _.size.x, _.size.y);
    
    _.orderDirection();
    _.intiAnimation();
  }
  private stopAnimation(){ // Detiene, animación de las lineas
    let _ = this;
    window.cancelAnimationFrame(_.animationId);
    clearTimeout(_.createLineId);
    _.lines = [];
  }
  private intiAnimation(){ // Inicializa los setinterval para ejecutar la animación 
    let _ = this;
    _.ngZone.runOutsideAngular(() => _.frames(_));
    _.createLineId = setTimeout(_.addArrayNewPosition.bind(null, _), 3000);
  }
  private orderDirection(){
    let _ = this;
    for (let i = 0; i < _.arrayDots.length; i++) {
      for (let j = 0; j < _.arrayDots[i].length; j++) {
        if(_.arrayDots[i][j].center !== null){
          if(_.arrayDots[i][j - 1] === undefined || _.arrayDots[i][j - 1].center === undefined || _.arrayDots[i][j - 1].center === null){
            _.arrayDots[i][j].top = false;
          }else{
            _.arrayDots[i][j].top = true;
          }
          if(_.arrayDots[i][j + 1] === undefined || _.arrayDots[i][j + 1].center === undefined || _.arrayDots[i][j + 1].center === null){
            _.arrayDots[i][j].bottom = false;
          }else{
            _.arrayDots[i][j].bottom = true;
          }
          if(_.arrayDots[i - 1] === undefined || _.arrayDots[i - 1][j].center === undefined || _.arrayDots[i - 1][j].center === null){
            _.arrayDots[i][j].left = false;
          }else{
            _.arrayDots[i][j].left = true;
          }
          if(_.arrayDots[i + 1] === undefined || _.arrayDots[i + 1][j].center === undefined || _.arrayDots[i + 1][j].center === null){
            _.arrayDots[i][j].right = false;
          }else{
            _.arrayDots[i][j].right = true;
          }
        }else{
          _.arrayDots[i][j].center = undefined;          
        }
      }
    }
  }
  private frames(_){  // Ejecuta la animación  de las lineas
    if(_ === undefined){return;}
    _.ctxLines.clearRect(0, 0, _.size.x, _.size.y);
    for(let i = 0; i < _.lines.length; i++){
      if(_.lines[i][0] !== undefined){
        _.ctxLines.fillStyle = (_.lines[i][0].long) > 0 ? "rgb(103, 127, 172)" : "rgba(103, 127, 172, " + (_.lines[i][0].fadeOut -= .01) + ")";
      }else{
        _.ctxLines.fillStyle = "rgb(103, 127, 172)";
      }
      for (let j = 0; j < _.lines[i].length; j++) {
        if(_.lines[i].length - 1 === j && (_.lines[i][j].size >= _.distance || _.lines[i][j].size <= (_.distance * -1))){// valida que sea el ultimo elemento del arreglo y que el tamaño de la linea haya llegado al otro punto
          if(_.lines[i][0].long > 0){       // valida si la animación de la linea continuará
            _.lines[i][0].long--;
            _.addNewPosition(_.lines[i][j].x, _.lines[i][j].y, _.lines[i][j].dir, i, j);
            if(_.lines[i][j + 1] === null){
              _.lines[i][0].long = 0;
              _.lines[i].splice(j + 1, 1);
              break;
            }
          }else{      // Si la animación de la linea ha llegado a su fin, se remueve y reinician los valores
            if(_.lines[i][0].fadeOut < 0.05){
              for (let a = 0; a < _.lines.length; a++) {
                for (let b = 0; b < _.lines[a].length; b++) {
                  _.arrayDots[_.lines[a][b].x][_.lines[a][b].y].animated = false;
                }
              }
              _.lines[i] = [];
              break;
            }
          }
        }
        switch (_.lines[i][j].dir) {
          case "top":
            _.ctxLines.fillRect((_.lines[i][j].x * _.distance) + (_.distance / 2) - 1, 
              (_.lines[i][j].y * _.distance) + _.residY - 1, 
              2, _.lines[i][j].size);
              _.lines[i][j].size = (_.lines[i][j].size < _.distance && _.lines[i][j].size > (_.distance * -1)) ? _.lines[i][j].size - 1 : _.lines[i][j].size;
            break;
          case "right":
            _.ctxLines.fillRect((_.lines[i][j].x * _.distance) + (_.distance / 2) - 1, 
              (_.lines[i][j].y * _.distance) + _.residY - 1, _.lines[i][j].size, 2);
              _.lines[i][j].size = (_.lines[i][j].size < _.distance && _.lines[i][j].size > (_.distance * -1)) ? _.lines[i][j].size + 1 : _.lines[i][j].size;
            break;
          case "bottom":
            _.ctxLines.fillRect((_.lines[i][j].x * _.distance) + (_.distance / 2) - 1, 
              (_.lines[i][j].y * _.distance) + _.residY - 1, 2, _.lines[i][j].size);
              _.lines[i][j].size = (_.lines[i][j].size < _.distance && _.lines[i][j].size > (_.distance * -1)) ? _.lines[i][j].size + 1 : _.lines[i][j].size;
            break;
          case "left":
            _.ctxLines.fillRect((_.lines[i][j].x * _.distance) + (_.distance / 2) - 1, 
              (_.lines[i][j].y * _.distance) + _.residY - 1, 
              _.lines[i][j].size, 2);
              _.lines[i][j].size = (_.lines[i][j].size < _.distance && _.lines[i][j].size > (_.distance * -1)) ? _.lines[i][j].size - 1 : _.lines[i][j].size;
            break;
        }
      }
    }
    for (let i = 0; i < _.lines.length; i++) {
      if(_.lines[i].length === 0){
        _.lines.splice(i, 1);
      }
    }
    _.animationId = window.requestAnimationFrame(_.frames.bind(null, _));
  }
  private addArrayNewPosition(_){
    if(_ === undefined){return;}
    if(_.lines.length < 10){
      let newPosition = _.getNewPosition();
      if(newPosition.length > 0){
        _.lines.push(newPosition);
      }else{
        _.lines.push(_.getNewPosition());
      }
    }
    _.createLineId = setTimeout(_.addArrayNewPosition.bind(null, _), (Math.random() * 1000) + 1000);
  }
  private getNewPosition(){     // genera los datos de la primera linea animada
    let _ = this;
    let point = [];
    let long = Math.floor(Math.random() * 2) + 2;       // longitud de la linea animada, minimo son 2, maximo son 5
    let dir = null;       // dirección en la que se moverá la linea 0=arriba,1=der,2=abajo,3=izq
    point.push(_.getPosition());
    dir = _.getDirection(point[0].x, point[0].y, ["top", "left", "right", "bottom"]);
    if(dir === null){return [];}
    _.arrayDots[point[0].x][point[0].y].animated = true;
    point[0]["size"] = 0;
    point[0]["long"] = long;
    point[0]["dir"] = dir;
    point[0]["fadeOut"] = 1;
    return point;
  }
  private getPosition(){ // obtiene un punto del grid
    let _ = this;
    let posX = Math.floor(Math.random() * _.numDot.x);
    let posY = Math.floor(Math.random() * _.arrayDots[posX].length);
    if(_.arrayDots[posX][posY] !== undefined && _.arrayDots[posX][posY].center !== null && (_.arrayDots[posX][posY].animated === undefined || !_.arrayDots[posX][posY].animated)){
      return {x: posX, y: posY};
    }else{
      return _.getPosition();
    }
  }
  private getDirection(x, y, arry){       // dirección en la que se moverá la linea, devuelve 0=arriba,1=der,2=abajo,3=izq
    let _ = this;
    let pos = Math.floor(Math.random() * arry.length);
    if(arry.length === 0 || _.arrayDots[x] === undefined || _.arrayDots[x][y] === undefined){return null;}
    let dir = arry[pos];
    switch (dir) {
      case "top":
        if(!_.arrayDots[x][y].top){
          arry.splice(pos, 1);
          dir = _.getDirection(x, y, arry);
        }
        break;
      case "right":
        if(!_.arrayDots[x][y].right){
          arry.splice(pos, 1);
          dir = _.getDirection(x, y, arry);
        }
        break;
      case "bottom":
        if(!_.arrayDots[x][y].bottom){
          arry.splice(pos, 1);
          dir = _.getDirection(x, y, arry);
        }
        break;
      case "left":
        if(!_.arrayDots[x][y].left){
          arry.splice(pos, 1);
          dir = _.getDirection(x, y, arry);
        }
        break;
    }   
    return dir;
  }
  private addNewPosition(x, y, dirPrev, posXLine, posYLine){
    let _ = this;
    let dir = null;
    let point = null;
    let orientation = ["top", "left", "right", "bottom"];
    if(_.arrayDots[x] === undefined || _.arrayDots[x][y] === undefined){return;}
    switch (dirPrev) {
      case "top":
        --y;
        orientation.splice(3, 1);
        orientation.splice(0, 1);
        break;
      case "right":
        ++x;
        orientation.splice(1, 2);
        break;
      case "bottom":
        ++y;
        orientation.splice(3, 1);
        orientation.splice(0, 1);
        break;
      case "left":
        --x;
        orientation.splice(1, 2);
        break;
    }
    dir = _.getDirection(x, y, orientation);
    _.arrayDots[x][y].animated = true;
    if(dir !== null){
      point = {
        x: x,
        y: y,
        size: 0,
        dir: dir
      };
    }
    _.lines[posXLine].push(point);
  }

}
























