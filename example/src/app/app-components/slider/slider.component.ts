import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent implements OnInit {

  @ViewChild('slide') slideRef: ElementRef; 
  @Input("data") banners: any = [];

  public dots: any = [];
  private loop: any = null;
  private slider: any = null;
  public counter: number = 0;
  private elemsSlider: any = null;
  private autoPlay: boolean = false;
  public animationData: any = {
    start: 0,
    status: false,
    top: 0
  };

  constructor(
  ){}

  ngOnInit() {
  }

  ngOnDestroy(){
    let _ = this;
    clearTimeout(_.loop);
  }

  public intiSlider(id: string, autoPlay?: boolean){
  	let _ = this;
  	_.slider = _.slideRef.nativeElement;
  	if(!_.slider){return;}
  	_.elemsSlider = _.slider.querySelectorAll(".slider");
    _.dots = new Array(_.elemsSlider.length);
    if(autoPlay){
    	_.autoPlay = autoPlay;

      _.slider.addEventListener("mouseenter", _.autoplay_stop.bind(_), false);
      _.slider.addEventListener("mouseleave", _.autoplay_play.bind(_), false);

    	_.loop = setTimeout(function(){
        if(_.elemsSlider.length > 0){
          _.next(-9);
        }
      }, 8000);
    }
    _.slider.addEventListener("touchstart", _.touchStartAnimation.bind(_), false);
    _.slider.addEventListener("touchend", _.touchEndAnimation.bind(_), false);
  }
  private touchStartAnimation(e){
    let _ = this;
    if(_.animationData.status){return;}
    _.animationData.status = true;
    _.animationData.start = e.touches[0].pageX;
    _.animationData.top = e.touches[0].pageY;
  }
  private touchEndAnimation(e){
    let _ = this;
    if(!_.animationData.status){return;}
    let diffY = _.animationData.top - e.changedTouches[0].pageY;
    let diffX = _.animationData.start - e.changedTouches[0].pageX;
    diffY = diffY < 0 ? diffY * -1 : diffY
    if(diffX > -50 && diffX < 50 || diffY > 150){
      _.animationData.status = false;
      return;
    }
    if(diffX < 0){
      _.next(-1);
    }else{
      _.next(-9);
    }
    _.animationData.status = false;
  }
  public next(mv){
    let _ = this;
    if(_.counter === mv){return;}
    if(_.autoPlay){
    	if(_.loop === null && _.counter !== mv){return;}
	    clearTimeout(_.loop);
	    _.loop = null;
	  }
    switch (mv) {
      case -9:
        _.counter = _.counter === _.elemsSlider.length - 1 ? 0 : _.counter + 1;
        break;
      case -1:
        _.counter = _.counter === 0 ? _.elemsSlider.length - 1 : _.counter - 1;
        break;
      default:
        _.counter = mv;
        break;
    }
    let active = _.slider.querySelector(".slider.show");
    let next = _.elemsSlider[_.counter];
    active.classList.remove("show");
    next.classList.add("show");

    let elemsDots = _.slider.querySelectorAll('.dots .dot');
    let activeDots = _.slider.querySelector('.dots .dot.active');
    activeDots.classList.remove("active");
    elemsDots[_.counter].classList.add("active");
    if(_.autoPlay){
    	_.loop = setTimeout(function(){
        if(_.elemsSlider.length > 0){
          _.next(-9);
        }
      }, 8000);
    }
  }
  private autoplay_stop(e){
    let _ = this;
    clearTimeout(_.loop);
    _.loop = undefined;
  }
  private autoplay_play(e){
    let _ = this;
    _.loop = setTimeout(function(){
      if(_.elemsSlider.length > 0){
        _.next(-9);
      }
    }, 8000);
  }
}







