import { Component, ViewChild, OnInit, AfterViewInit } from '@angular/core';

import { GridComponent } from './app-components/grid/grid.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit{
  
	@ViewChild('grid') grid: GridComponent;

	private config_grid: any = {
		animation: true,
	  colorDots: "#ffffff",
	  full: true,
	  elements: [
	    
	  ]
	};

  constructor(
 	){}

 	ngAfterViewInit(){
 		let _ = this;
 		console.log(_.grid);
 		// _.grid.getSizeX();
    _.grid.getSizeY();
    _.grid.generateGrid();
 	}
}









