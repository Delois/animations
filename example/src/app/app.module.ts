import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { SliderComponent } from './app-components/slider/slider.component';
import { GridComponent } from './app-components/grid/grid.component';

@NgModule({
  declarations: [
	  SliderComponent,
		GridComponent,
    AppComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
