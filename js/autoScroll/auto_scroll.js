/*
 *	content:: contenedor del scroll
 *	target:: punto de traslado
 *	duration:: tiempo de traslación
 */

function smoothScroll(content, target, duration){
	let content_target = document.querySelector(content);
	let move_target = document.querySelector(target);
	let targetPosition = move_target.offsetTop;
	let startPosition = content_target.scrollTop;
	let distance = targetPosition - startPosition;
	let startTime = null;
	
	function animation(currenTime){
		if(!startTime) {startTime = currenTime;}
		let timeElapsed = currenTime - startTime;
		let run = ease(timeElapsed, startPosition, distance, duration);
		content_target.scrollTo(0, run);
		if(timeElapsed < duration) {requestAnimationFrame(animation);}
	}

	function ease(t, b, c, d){
		t /= d/2;
		if(t < 1){return c / 2 * t * t + b;}
		t--;
		return -c / 2 * (t * (t - 2) - 1) + b;
	}

	requestAnimationFrame(animation);
}




